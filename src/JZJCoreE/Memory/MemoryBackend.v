module MemoryBackend//Contains addressing logic for MemoryController to choose between various memory backends
#(
	parameter INITIAL_RAM_CONTENTS = "initialRam.mem",
	parameter A_WIDTH = 12
)
(//Note: addresses are only 30 bits because they are wordwise addresses
	input clock,
	input reset,
	
	//Addressing
	input [29:0] address,
	
	//Memory stores
	input [31:0] dataIn,
	input writeEnable,
	
	//Memory loads
	output [31:0] dataOut,
	
	//Instruction stuff
	output [31:0] instructionOut,
	input [29:0] instructionAddress,
	
	//CPU memory mapped ports
	//Reads from the address read from the input, writes write to the output
	//Inputs: (word-wise read)		address
	input [31:0] portAInput,//		3FFFFFF8
	input [31:0] portBInput,//		3FFFFFF9
	input [31:0] portCInput,//		3FFFFFFA
	input [31:0] portDInput,//		3FFFFFFB
	input [31:0] portEInput,//		3FFFFFFC
	input [31:0] portFInput,//		3FFFFFFD
	input [31:0] portGInput,//		3FFFFFFE
	input [31:0] portHInput,//		3FFFFFFF
	//Outputs: (word-wise write)	address
	output [31:0] portAOutput,//	3FFFFFF8
	output [31:0] portBOutput,//	3FFFFFF9
	output [31:0] portCOutput,//	3FFFFFFA
	output [31:0] portDOutput,//	3FFFFFFB
	output [31:0] portEOutput,//	3FFFFFFC
	output [31:0] portFOutput,//	3FFFFFFD
	output [31:0] portGOutput,//	3FFFFFFE
	output [31:0] portHOutput//	3FFFFFFF
	//For tristate ports, an additional port's outputs can be designated as a direction register, which can be handled by and external module
	//If feedback is desired, then inputs should be connected to their respective output register
	//MAKE SURE INPUTS ARE SYNCHRONIZED IF THEY ARE FROM ANOTHER CLOCK DOMAIN
);
/* Wires and Assignments */

wor [31:0] combinedDataOut;
assign dataOut = combinedDataOut;

wire [31:0] ramDataOut;
wire [31:0] ioDataOut;
//Wor assignment
assign combinedDataOut = ramDataOut;
assign combinedDataOut = ioDataOut;

/* Backend backends (handle bounds checking themselves) */

//Ram
RAMWrapper #(.INITIAL_RAM_CONTENTS(INITIAL_RAM_CONTENTS), .A_WIDTH(A_WIDTH)) ram
				(.clock(clock), .address(address), .dataIn(dataIn), .dataOut(ramDataOut), .writeEnable(writeEnable), .instructionOut(instructionOut), .instructionAddress(instructionAddress));

//Memory mapped IO
MemoryMappedIOManager memoryMappedIO (.clock(clock), .reset(reset), .address(address), .dataIn(dataIn), .dataOut(ioDataOut), .writeEnable(writeEnable), .portAInput(portAInput),
												  .portBInput(portBInput), .portCInput(portCInput), .portDInput(portDInput), .portEInput(portEInput), .portFInput(portFInput), .portGInput(portGInput),
												  .portHInput(portHInput), .portAOutput(portAOutput), .portBOutput(portBOutput), .portCOutput(portCOutput), .portDOutput(portDOutput),
												  .portEOutput(portEOutput), .portFOutput(portFOutput), .portGOutput(portGOutput), .portHOutput(portHOutput));

endmodule 
