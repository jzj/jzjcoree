module ValueFormer//Forms 32 bit values to be directly stored in rd for lui and auipc instructions
(
	//Outputting
	output reg [31:0] valueOut,
	input outputEnable,
	input operation,//0 indicates lui; 1 indicates auipc
	
	//Input data
	input [31:0] pc,//for auipc (contains the address of the currently executing instruction)
	input [31:0] immediateU//for lui and auipc
);

/* Value Forming Logic */

always @*
begin
	if (outputEnable)
	begin
		if (operation == 1'b0)//lui
			valueOut = immediateU;
		else//auipc
			valueOut = immediateU + pc;
	end
	else
		valueOut = 32'h00000000;//not writing anything
end

endmodule