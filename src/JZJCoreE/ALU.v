module ALU//Does logical and arithemetic operations that aren't related to jumping or branching
(
	//Output
	output [31:0] resultOut,
	input outputEnable,
	
	//Registers and immediates
	input [31:0] portRS1,
	input [31:0] portRS2,
	input [31:0] immediateI,
	
	//Alu operation type
	input [2:0] funct3,//Determines the type of operation we are performing
	input opImm,//set to 1 if the operation should be between portRS1 and immediateI instead of portRS2 (OP-IMM opcode type)
	input [6:0] funct7
);
/* ALU interface */
//Note: m extention functionality is disabled because it is too slow and consumes too many logic elements

//Output and output enable logic
reg [31:0] result;
assign resultOut = outputEnable ? result : 32'h00000000;//only output if enabled

//Renaming for convenience and mux between portRS2 and immediateI
wire [31:0] valueX = portRS1;
wire [31:0] valueY = opImm ? immediateI : portRS2;

//funct7 interpretation//todo check for funct7 errors
wire baseOperationModifier = funct7[5];//sub instead of add or sra instead of srl
wire subtract = baseOperationModifier & !opImm;//subtractions only happen in register-register operations
//wire mExtensionInstruction = funct7[0];//enabled for multiplication and divisions instructions of the risc v m extension

//Perform operation
always @*
begin
	/*
	if (mExtensionInstruction)
	begin
		case (funct3)
			3'b000: result = sharedSignedMultiplyOutput[31:0];//mul
			3'b001: result = sharedSignedMultiplyOutput[63:32];//mulh
			3'b010: result = mulhsuIntermediateOutput[63:32];//mulhsu
			3'b011: result = mulhuIntermediateOutput[63:32];//mulhu
			3'b100: result = divide32(valueX, valueY, 1);//div
			3'b101: result = divide32(valueX, valueY, 0);//divu
			3'b110: result = remainder32(valueX, valueY, 1);//rem
			3'b111: result = remainder32(valueX, valueY, 0);//remu
			default: result = 32'h00000000;//will never happen
		endcase
	end
	else//Is an alu operation from the base spec
	begin
	*/
		case (funct3)//Determines the type of operation we are performing
			3'b000: result = addSub32(valueX, valueY, subtract);//add/addi/sub
			3'b001: result = shiftLeftLogical32By5(valueX, valueY[4:0]);//sll/slli
			3'b010: result = setIfLessThan32(valueX, valueY, 1);//slt/slti
			3'b011: result = setIfLessThan32(valueX, valueY, 0);//sltu/sltiu
			3'b100: result = valueX ^ valueY;//xor/xori
			3'b101: result = shiftRight32By5(valueX, valueY, baseOperationModifier);//srl/srli/sra/srai
			3'b110: result = valueX | valueY;//or/ori
			3'b111: result = valueX & valueY;//and/andi
			default: result = 32'hxxxxxxxx;//will never happen
		endcase
	/*
	end
	*/
end

/* Math functions */

//Note: simple functions (==, |, &, ^, !=, etc) won't be found here because they are trivial to implement and so they
//wouldn't be a good learning excersise/use of time
//Also even those I have implemented these, my code is commented out because that way I don't sacrifice speed

function automatic [31:0] addSub32(input [31:0] a, input [31:0] b, input subAB);//add/addi/sub
//My own implementation
/*
integer i;
reg [31:0] bActual;
reg [32:0] carryIn;//Note: bit 32 goes unused
reg [32:0] ABCarryIn;//Note: bit 32 goes unused
reg [32:0] BCinCarryIn;//Note: bit 32 goes unused
reg [31:0] sumBCin;
begin
	//Conversion to twos complement for subtraction
	bActual = subAB ? ~b : b;//Invert all bits if subtracting
	carryIn[0] = subAB;//First carry in coming from subAB; effectively adding 1 if subtracting

	//Adding logic
	for (i = 0; i < 32; i = i + 1)
	begin
		  //Half adder 1 between b and carry in
		  sumBCin[i] = bActual[i] ^ carryIn[i];
		  BCinCarryIn[i+1] = bActual[i] & carryIn[i];//The carry in to the next full adder will be based on this
		  
		  //Half adder 2 between a and the sum of b and carry in
		  addSub32[i] = a[i] ^ sumBCin[i];
		  ABCarryIn[i+1] = a[i] & sumBCin[i];//The carry in to the next full adder will be based on this
		  
		  //Next carry is 1 if either half adder 1 or half adder 2 had an overflow (impossible for both to have one at the same time)
		  carryIn[i+1] = BCinCarryIn[i+1] | ABCarryIn[i+1];
	end
end
*/
//Implementation with verilog operators
begin
	addSub32 = subAB ? a - b : a + b;
end
endfunction

//todo implement these myself for practice instead of relying on verilog operators

function automatic [31:0] shiftLeftLogical32By5(input [31:0] a, input [4:0] b);//sll/slli
begin
	shiftLeftLogical32By5 = a << b;
end
endfunction

function automatic [31:0] shiftRight32By5(input [31:0] a, input [4:0] b, input isArithmeticShift);//srl/srli/sra/srai
begin
	shiftRight32By5 =	isArithmeticShift ? a >>> b : a >> b;
end
endfunction

function automatic [31:0] setIfLessThan32(input [31:0] a, input [31:0] b, input isSigned);//slt/slti/sltu/sltiu
begin
	if (isSigned)
		setIfLessThan32 = ($signed(a) < $signed(b)) ? 32'h00000001 : 32'h00000000;
	else//unsigned
		setIfLessThan32 = (a < b) ? 32'h00000001 : 32'h00000000;
end
endfunction

//For now these functions are too slow and greedy for luts
/* 
function automatic [63:0] multiply32Out64(input [31:0] a, input [31:0] b, input isSigned);//mul/mulh/mulhu
begin
	if (isSigned)
		multiply32Out64 = $signed(a) * $signed(b);
	else//unsigned
		multiply32Out64 = a * b;
end
endfunction

function automatic [63:0] multiply32Out64SignedByUnsigned(input [31:0] a, input [31:0] b);//mul/mulh/mulhu
begin
	multiply32Out64SignedByUnsigned = $signed(a) * b;//a is signed, b is unsigned
end
endfunction

function automatic [31:0] divide32(input [31:0] a, input [31:0] b, input isSigned);//div/divu
begin
	if (isSigned)
		divide32 = $signed(a) / $signed(b);
	else//unsigned
		divide32 = a / b;
end
endfunction

function automatic [31:0] remainder32(input [31:0] a, input [31:0] b, input isSigned);//rem/remu
begin
	if (isSigned)
		remainder32 = $signed(a) % $signed(b);
	else//unsigned
		remainder32 = a % b;
end
endfunction
*/

endmodule